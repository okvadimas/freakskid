<?php

namespace App\Controllers;

class About extends BaseController
{
    public function index()
    {
        $data = [
            "title" => "About",
            "aktif" => "about"
        ];
        return view("/about/about.php", $data);
    }

    //--------------------------------------------------------------------

}
