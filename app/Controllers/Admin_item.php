<?php

namespace App\Controllers;

use \App\Models\Models;

class Admin_item extends BaseController
{
    protected $itemModel;

    public function __construct()
    {
        $this->itemModel = new Models();
    }
    public function index()
    {

        if (!session()->get("username")) {
            return redirect()->to("/login/");
        }

        $faker = \Faker\Factory::create();
        // dd($faker);

        // kalau di get url ada angka nya maka pake itu dan klw tidak pakai angka 1
        $current_page = $this->request->getVar("page_tabel_detail") ? $this->request->getVar("page_tabel_detail") : 1;

        $keyword = $this->request->getVar("keyword");
        if ($keyword) {
            // ini function search ada di Models
            $item = $this->itemModel->search($keyword)->orderBy("id", "desc")->paginate(12, "tabel_detail");
            // dd($item);
        } else {
            $item = $this->itemModel->orderBy("id", "desc")->paginate(12, "tabel_detail");
        };
        // d($this->request->getVar("keyword"));

        // $item = $this->itemModel->paginate(12, "tabel_detail");
        // ini utk menampilkan linknya
        $pager = $this->itemModel->pager;
        // dd($item);

        $data = [
            "items" => $item,
            "pager" => $pager,
            // utk meninggalkan pesan ketika mencari melaui keyword search
            "keyword" => $this->request->getVar("keyword"),
            "current_page" => $current_page,
            "validation" => \Config\Services::validation(),
        ];
        return view("/admin/admin_item", $data);
    }

    public function insert()
    {
        $data = [
            "title" => "Insert Page",
            "validation" => \Config\Services::validation(),
        ];
        return view("/admin/insert_item", $data);
    }

    public function save()
    {
        if (!$this->validate([
            "nama_barang" => "required|is_unique[tabel_detail.nama_barang]",
            "merk" => "required",
            "kondisi" => "required",
            "lokasi" => "required",
            "harga" => "required",
            "stok" => "required",
            "barang" => "required",
            "kontak" => "required",
            // "gambar" => "required|is_unique[tabel_detail.gambar]",
            "gambar" => "required",
        ])) {
            $validation = \Config\Services::validation();
            session()->setFlashdata("pesan_gagal", "Failed Insert New Data");
            // dd($validation);
            return redirect()->to("/admin_item")->withInput()->with("validation", $validation);
        };

        // dd($this->request->getVar());
        $this->itemModel->save([
            "nama_barang" => $this->request->getVar("nama_barang"),
            "merk" => $this->request->getVar("merk"),
            "kondisi" => $this->request->getVar("kondisi"),
            "lokasi" => $this->request->getVar("lokasi"),
            "harga" => $this->request->getVar("harga"),
            "stok" => $this->request->getVar("stok"),
            "barang" => $this->request->getVar("barang"),
            "kontak" => $this->request->getVar("kontak"),
            "gambar" => $this->request->getVar("nama_barang"),
        ]);

        session()->setFlashdata("pesan", "Success Insert New Data");

        return redirect()->to("/admin_item");
    }

    public function delete($id)
    {
        $this->itemModel->delete($id);
        session()->setFlashdata("pesan", "Success Delete Data");
        return redirect()->to("/admin_item");
    }

    public function edit($id)
    {
        $item = $this->itemModel->where(["id" => $id])->first();

        $data = [
            "title" => "Edit Page",
            "item" => $item,
            "validation" => \Config\Services::validation(),
        ];

        return view("/admin/edit_item", $data);
    }

    public function update($id)
    {
        $itemLama = $this->itemModel->where(["id" => $id])->first();
        if ($itemLama["nama_barang"] == $this->request->getVar("nama_barang")) {
            $rule = "required";
        } else {
            $rule = "required|is_unique[tabel_detail.nama_barang]";
        };

        if (!$this->validate([
            "nama_barang" => $rule,
            "merk" => "required",
            "kondisi" => "required",
            "lokasi" => "required",
            "harga" => "required",
            "stok" => "required",
            "barang" => "required",
            "kontak" => "required",
            // "gambar" => "required|is_unique[tabel_detail.gambar]",
            "gambar" => "required",
        ])) {
            $validation = \Config\Services::validation();
            // dd($validation);
            return redirect()->to("/admin_item/edit/" . $id)->withInput()->with("validation", $validation);
        };

        // dd($this->request->getVar());
        $this->itemModel->save([
            "id" => $id,
            "nama_barang" => $this->request->getVar("nama_barang"),
            "merk" => $this->request->getVar("merk"),
            "kondisi" => $this->request->getVar("kondisi"),
            "lokasi" => $this->request->getVar("lokasi"),
            "harga" => $this->request->getVar("harga"),
            "stok" => $this->request->getVar("stok"),
            "barang" => $this->request->getVar("barang"),
            "kontak" => $this->request->getVar("kontak"),
            "gambar" => $this->request->getVar("nama_barang"),
        ]);

        session()->setFlashdata("pesan", "Success Update New Data");

        return redirect()->to("/admin_item");
    }
    //--------------------------------------------------------------------

}
