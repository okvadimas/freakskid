<?php

namespace App\Controllers;

class Contact extends BaseController
{
    public function index()
    {
        $data = [
            "title" => "Contact",
            "aktif" => "contact"
        ];
        return view("/contact/contact.php", $data);
    }

    //--------------------------------------------------------------------

}
