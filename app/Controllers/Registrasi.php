<?php

namespace App\Controllers;

use App\Models\LoginModels;
use \App\Models\Models;

class Registrasi extends BaseController
{
    protected $loginModel;

    public function __construct()
    {
        $this->loginModel = new LoginModels();
    }

    public function index()
    {
        // $orang = $this->loginModel->findAll();
        // dd($orang);

        $data = [
            "title" => "Registrasi Page",
            "validation" => \Config\Services::validation(),
        ];
        return view("/registrasi/registrasi.php", $data);
    }

    public function registrasi()
    {
        $encrypter = \Config\Services::encrypter();

        if (!$this->validate([
            "username" => "required|is_unique[orang.username]",
            "email" => "required|is_unique[orang.email]|valid_email",
            "password" => "required",
            "password2" => "required|matches[password]",
        ])) {
            $validation = \Config\Services::validation();
            session()->setFlashdata("pesan_gagal", "Failed Create New Account");
            // dd($validation);
            return redirect()->to("/registrasi")->withInput()->with("validation", $validation);
        };

        $this->loginModel->save([
            "username" => $this->request->getVar("username"),
            "email" => $this->request->getVar("email"),
            // "password" => $encrypter->encrypt($this->request->getVar("password")),
            "password" => password_hash($this->request->getVar("password"), PASSWORD_DEFAULT),
            "level" => "2",
        ]);

        session()->setFlashdata("pesan", "Success Create New Account");
        return redirect()->to("/login");
    }

    public function logout()
    {
        session()->destroy();
        return redirect()->to("/item");
    }

    //--------------------------------------------------------------------

}
