<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		$data = [
			"title" => "Home",
			"aktif" => "home",
		];
		return view("/home/index.php", $data);
	}

	//--------------------------------------------------------------------

}
