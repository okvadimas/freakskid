<?php

namespace App\Controllers;

class Work extends BaseController
{
    public function index()
    {
        $data = [
            "title" => "Work",
            "aktif" => "work"
        ];
        return view("/work/work.php", $data);
    }

    //--------------------------------------------------------------------

}
