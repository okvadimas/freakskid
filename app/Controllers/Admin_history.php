<?php

namespace App\Controllers;

use \App\Models\Models_penjualan;
use CodeIgniter\I18n\Time;

class Admin_history extends BaseController
{
    protected $itemModel;

    public function __construct()
    {
        $faker = \Faker\Factory::create();
        $this->itemModel = new Models_penjualan();
    }
    public function index()
    {
        if (!session()->get("username")) {
            return redirect()->to("/login/");
        }

        $current_page = $this->request->getVar("page_tabel_penjualan") ? $this->request->getVar("page_tabel_penjualan") : 1;

        $keyword = $this->request->getVar("keyword");
        if ($keyword) {
            // ini function search ada di Models
            $item = $this->itemModel->search($keyword)->orderBy("id", "desc")->paginate(12, "tabel_penjualan");
            // dd($item);
        } else {
            $item = $this->itemModel->orderBy("id", "desc")->paginate(12, "tabel_penjualan");
        };
        // d($this->request->getVar("keyword"));

        // $item = $this->itemModel->paginate(12, "tabel_detail");
        // ini utk menampilkan linknya
        $pager = $this->itemModel->pager;
        // dd($item);

        $data = [
            "items" => $item,
            "pager" => $pager,
            // utk meninggalkan pesan ketika mencari melaui keyword search
            "keyword" => $this->request->getVar("keyword"),
            "current_page" => $current_page,
            "validation" => \Config\Services::validation(),
        ];
        return view("/admin/admin_history", $data);
    }

    // sudah tidak terpakai sekarang pakai modal 
    // public function insert()
    // {
    //     $data = [
    //         "title" => "Insert History Page",
    //         "validation" => \Config\Services::validation(),
    //     ];
    //     return view("/admin/insert_history", $data);
    // }

    public function save()
    {
        // dd($this->request->getVar());
        if (!$this->validate([
            "nama_barang" => "required|is_unique[tabel_penjualan.nama_barang]",
            "merk" => "required",
            "total" => "required",
            "sumber" => "required",
            "tujuan" => "required",
            "harga_beli" => "required",
            "harga_jual" => "required",
            // "gambar" => "required|is_unique[tabel_detail.gambar]",
        ])) {
            $validation = \Config\Services::validation();
            session()->setFlashdata("pesan_gagal", "Failed Insert New Data");
            // dd($validation);
            return redirect()->to("/admin_history/")->withInput()->with("validation", $validation);
        };

        // dd($this->request->getVar());
        $this->itemModel->save([
            "nama_barang" => $this->request->getVar("nama_barang"),
            "merk" => $this->request->getVar("merk"),
            "total" => $this->request->getVar("total"),
            "sumber" => $this->request->getVar("sumber"),
            "tujuan" => $this->request->getVar("tujuan"),
            "harga_beli" => $this->request->getVar("harga_beli"),
            "harga_jual" => $this->request->getVar("harga_jual"),
            "waktu" => Time::now(),
        ]);

        session()->setFlashdata("pesan", "Success Insert New Data");

        return redirect()->to("/admin_history");
    }

    public function delete($id)
    {
        $this->itemModel->delete($id);
        session()->setFlashdata("pesan", "Success Delete Data");
        return redirect()->to("/admin_history");
    }

    public function edit($id)
    {
        $item = $this->itemModel->where(["id" => $id])->first();
        // dd($item);
        $data = [
            "title" => "Edit History Page",
            "item" => $item,
            "validation" => \Config\Services::validation(),
        ];

        return view("/admin/edit_history", $data);
    }

    public function update($id)
    {
        $itemLama = $this->itemModel->where(["id" => $id])->first();
        if ($itemLama["nama_barang"] == $this->request->getVar("nama_barang")) {
            $rule = "required";
        } else {
            $rule = "required|is_unique[tabel_penjualan.nama_barang]";
        };

        if (!$this->validate([
            "nama_barang" => $rule,
            "merk" => "required",
            "total" => "required",
            "sumber" => "required",
            "tujuan" => "required",
            "harga_beli" => "required",
            "harga_jual" => "required",
        ])) {
            $validation = \Config\Services::validation();
            // dd($validation);
            return redirect()->to("/admin_history/edit/" . $id)->withInput()->with("validation", $validation);
        };

        // dd($this->request->getVar());
        $this->itemModel->save([
            "id" => $id,
            "nama_barang" => $this->request->getVar("nama_barang"),
            "merk" => $this->request->getVar("merk"),
            "total" => $this->request->getVar("total"),
            "sumber" => $this->request->getVar("sumber"),
            "tujuan" => $this->request->getVar("tujuan"),
            "harga_beli" => $this->request->getVar("harga_beli"),
            "harga_jual" => $this->request->getVar("harga_jual"),
            "waktu" => Time::now(),
        ]);

        session()->setFlashdata("pesan", "Success Update New Data");

        return redirect()->to("/admin_history");
    }

    //--------------------------------------------------------------------

}
