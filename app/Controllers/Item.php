<?php

namespace App\Controllers;

use \App\Models\Models;

class Item extends BaseController
{
    protected $itemModel;

    public function __construct()
    {
        $this->itemModel = new Models();
    }

    public function index()
    {
        // dd($this->itemModel);
        $faker = \Faker\Factory::create();
        // dd($faker);

        $keyword = $this->request->getVar("keyword");
        if ($keyword) {
            // ini function search ada di Models
            $item = $this->itemModel->search($keyword)->orderBy("id", "desc")->paginate(12, "tabel_detail");
            // dd($item);
        } else {
            $item = $this->itemModel->orderBy("id", "desc")->paginate(12, "tabel_detail");
        };
        // d($this->request->getVar("keyword"));

        // $item = $this->itemModel->paginate(12, "tabel_detail");
        // ini utk menampilkan linknya
        $pager = $this->itemModel->pager;
        // dd($item);

        $data = [
            "title" => "Item",
            "aktif" => "item",
            // "items" => $item,
            "items" => $item,
            "pager" => $pager,
            // utk meninggalkan pesan ketika mencari melaui keyword search
            "keyword" => $this->request->getVar("keyword"),
        ];
        return view("/item/item.php", $data);
    }



    //--------------------------------------------------------------------

}
