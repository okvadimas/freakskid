<?php

namespace App\Controllers;

class Admin_profile extends BaseController
{
    public function index()
    {
        $data = [
            "title" => "Profile Page",
        ];
        return view("/admin/profile.php", $data);
    }

    //--------------------------------------------------------------------

}
