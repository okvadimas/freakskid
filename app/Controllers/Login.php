<?php

namespace App\Controllers;

use App\Models\LoginModels;
use \App\Models\Models;

class Login extends BaseController
{
    protected $loginModel;

    public function __construct()
    {
        $this->loginModel = new LoginModels();
    }

    public function index()
    {
        // $orang = $this->loginModel->findAll();
        // dd($orang);

        $data = [
            "title" => "Login Page",
        ];
        return view("/login/login.php", $data);
    }

    public function login()
    {
        $encrypter = \Config\Services::encrypter();

        $orang = $this->loginModel->where(["username" => $this->request->getVar("username")])->orWhere(["email" => $this->request->getVar("username")])->first();
        // dd($orang["password"], password_verify($this->request->getVar("password"), $orang["password"]));

        if (isset($orang)) {
            if (password_verify($this->request->getVar("password"), $orang["password"])) {
                session()->set("username", $orang["username"]);
                session()->set("email", $orang["email"]);
                session()->set("level", $orang["level"]);
                return redirect()->to("/admin_history");
            } else {
                session()->setFlashdata("pesan_gagal", "username or password invalid");
                return redirect()->to("/login/");
            }
        } else {
            session()->setFlashdata("pesan_gagal", "username or password invalid");
            return redirect()->to("/login/");
        }
    }

    public function logout()
    {
        session()->destroy();
        return redirect()->to("/item");
    }

    //--------------------------------------------------------------------

}
