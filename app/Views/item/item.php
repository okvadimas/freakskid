<?php $this->extend("/layout/template"); ?>
<?php $this->section("content"); ?>

<section class="hero-wrap hero-wrap-2 degree-right" style="background-image: url('images/bg_2.jpg');" data-stellar-background-ratio="0.5">
  <div class="overlay"></div>
  <div class="container">
    <div class="row no-gutters slider-text js-fullheight align-items-end">
      <div class="col-md-9 ftco-animate pb-5 mb-5">
        <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home <i class="fa fa-chevron-right"></i></a></span> <span>Item <i class="fa fa-chevron-right"></i></span></p>
        <h1 class="mb-3 bread">Item</h1>
      </div>
    </div>
  </div>
</section>


<section class="ftco-section">

  <div class="container">
    <form action="" method="post">
      <div class="input-group mb-3">
        <input type="text" class="form-control btn-outline-white" placeholder="Searching Item Here..." name="keyword" aria-describedby="button-addon2" value="<?= ($keyword) ? $keyword : "" ?>">
        <div class="input-group-append">
          <button class="btn btn-outline-dark" type="submit" name="submit" id="button-addon2"><i class="fa fa-search fa-2x" aria-hidden="true"></i></button>
        </div>
      </div>
    </form>
    <div class="row d-flex">
      <?php foreach ($items as $item) : ?>
        <div class="col-md-3 d-flex ftco-animate">
          <div class="blog-entry justify-content-end">
            <div class="text">
              <a class="block-20 img" style="background-image: url('images/<?= $item["gambar"]; ?>');"></a>
              <div>
                <p class="m-0 p-0 text-dark"><?= $item["merk"]; ?></p>
                <p class="m-0 p-0 text-body"><?= $item["nama_barang"]; ?></p>
                <p class="m-0 p-0 text-body"><?= $item["kondisi"]; ?></p>
                <p class="m-0 p-0 text-dark">Lokasi <?= $item["lokasi"]; ?></p>
                <p class="m-0 p-0 text-dark"><?= $item["harga"]; ?></p>
                <p class="m-0 p-0 text-dark"><?= $item["barang"]; ?> <?= $item["stok"]; ?> Item</p>
                <p class="m-0 p-0 text-dark mt-2"><a target="_blank" href="https://api.whatsapp.com/send?phone=6288211234125&text=Saya ingin memesan produk <?= $item["nama_barang"]; ?> mohon bantuanya"><button class="btn btn-success"><i class="fa fa-whatsapp"></i> Whatsapp</button></a></p>
              </div>
            </div>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
    <?= $pager->links("tabel_detail", "item_pagination"); ?>
    <!-- <div class="row mt-5 pb-5">
      <div class="col text-center">
        <div class="block-27">
          <ul>
            <li><a href="#">&lt;</a></li>
            <li class="active"><span>1</span></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">&gt;</a></li>
          </ul>
        </div>
      </div>
    </div> -->
  </div>
</section>

<?php $this->endSection(); ?>