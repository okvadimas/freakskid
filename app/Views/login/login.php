<!DOCTYPE html>
<html lang="en">

<head>
	<title><?= $title; ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/jpg" href="/images/icon.jpg" />
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/assets/icons/font-awesome/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/css/material-design-iconic-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/css/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/css/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/css/animsition.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/css/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/css/daterangepicker.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/css/util.css">
	<link rel="stylesheet" type="text/css" href="/css/main.css">
	<!--===============================================================================================-->
</head>

<body>

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form" action="/login/login" method="post">
					<span class="login100-form-title p-b-26">
						Welcome
					</span>
					<span class="login100-form-title p-b-48">
						<i><img src="/images/icon.jpg" width="100px" alt=""></i>
					</span>

					<div class="wrap-input100 validate-input" data-validate="Enter Username">
						<input class="input100" type="text" name="username">
						<span class="focus-input100" data-placeholder="Username or Email"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" type="password" name="password">
						<span class="focus-input100" data-placeholder="Password"></span>
					</div>

					<?php if (session()->getFlashdata("pesan_gagal")) : ?>
						<div class="alert alert-danger fade show text-center" role="alert">
							<?= session()->getFlashdata("pesan_gagal"); ?>
						</div>
					<?php endif; ?>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn" name="login">
								Sign In
							</button>
						</div>
					</div>

					<div class="text-center p-t-50">
						<span class="txt1">
							Don’t have an account?
						</span>

						<a class="txt2" href="/registrasi/">
							Sign Up
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>


	<div id="dropDownSelect1"></div>

	<!--===============================================================================================-->
	<script src="/js/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script src="/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script src="/js//popper.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="/js/select2.min.js"></script>
	<!--===============================================================================================-->
	<script src="/js/moment.min.js"></script>
	<script src="/js/daterangepicker.js"></script>
	<!--===============================================================================================-->
	<script src="/js/countdowntime.js"></script>
	<!--===============================================================================================-->
	<script src="/js/mainLogin.js"></script>

</body>

</html>