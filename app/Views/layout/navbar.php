<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="/">a<span>v</span>o</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item <?= ($aktif == "home") ? "active" : "" ?>"><a href="/" class="nav-link">Home</a></li>
                <li class="nav-item <?= ($aktif == "about") ? "active" : "" ?>"><a href="/about" class="nav-link">About</a></li>
                <li class="nav-item <?= ($aktif == "work") ? "active" : "" ?>"><a href="/work" class="nav-link">Work</a></li>
                <li class="nav-item <?= ($aktif == "blog") ? "active" : "" ?>"><a href="/item" class="nav-link">Item</a></li>
                <li class="nav-item <?= ($aktif == "contact") ? "active" : "" ?>"><a href="/contact" class="nav-link">Contact</a></li>
                <li class="nav-item <?= ($aktif == "admin") ? "active" : "" ?>"><a href="/admin_history" class="nav-link">Admin</a></li>
            </ul>
        </div>
    </div>
</nav>
<!-- END nav -->