<!DOCTYPE html>
<html lang="en">

<head>
	<title><?= $title; ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/jpg" href="/images/icon.jpg" />
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/assets/icons/font-awesome/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/css/material-design-iconic-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/css/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/css/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/css/animsition.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/css/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/css/daterangepicker.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/css/util.css">
	<link rel="stylesheet" type="text/css" href="/css/main.css">
	<!--===============================================================================================-->
</head>

<body>

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form" action="/registrasi/registrasi" method="post">
					<span class="login100-form-title p-b-26">
						Welcome
					</span>
					<span class="login100-form-title p-b-48">
						<i><img src="/images/icon.jpg" width="100px" alt=""></i>
					</span>
					<input class="input100 <?= ($validation->hasError('username')) ? 'is-invalid' : '' ?>" type="hidden" name="username" value="<?= old("username"); ?>">

					<div class="wrap-input100 validate-input" data-validate="Enter Username">
						<input class="input100 <?= ($validation->hasError('username')) ? 'is-invalid' : '' ?>" type="text" name="username">
						<span class="focus-input100" data-placeholder="Username" value="<?= old("username"); ?>"></span>
						<div class="invalid-feedback">
							<?= $validation->getError("username"); ?>
						</div>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter Email">
						<input class="input100 <?= ($validation->hasError("email")) ? 'is-invalid' : '' ?>" type="email" name="email">
						<span class="focus-input100" data-placeholder="Email"></span>
						<div class="invalid-feedback">
							<?= $validation->getError("email"); ?>
						</div>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<span class="btn-show-pass ">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100 <?= ($validation->hasError("password")) ? 'is-invalid' : '' ?>" type="password" name="password">
						<span class="focus-input100" data-placeholder="Password"></span>
						<div class="invalid-feedback">
							<?= $validation->getError("password"); ?>
						</div>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100 <?= ($validation->hasError("password2")) ? 'is-invalid' : '' ?>" type="password" name="password2">
						<span class="focus-input100" data-placeholder="Confirm Password"></span>
						<div class="invalid-feedback">
							<?= $validation->getError("password2"); ?>
						</div>
					</div>

					<?php if (isset($error)) : ?>
						<p class="text-center" style="color: red; font-style: italic;">Username / Password Incorrect!</p>
					<?php endif; ?>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn" name="registrasi">
								Sign Up
							</button>
						</div>
					</div>

					<div class="text-center p-t-50">
						<span class="txt1">
							Already have an account?
						</span>

						<a class="txt2" href="/login/">
							Sign In
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>


	<div id="dropDownSelect1"></div>

	<!--===============================================================================================-->
	<script src="/js/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script src="/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script src="/js//popper.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="/js/select2.min.js"></script>
	<!--===============================================================================================-->
	<script src="/js/moment.min.js"></script>
	<script src="/js/daterangepicker.js"></script>
	<!--===============================================================================================-->
	<script src="/js/countdowntime.js"></script>
	<!--===============================================================================================-->
	<script src="/js/mainLogin.js"></script>

</body>

</html>