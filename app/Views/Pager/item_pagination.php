<!-- <div class="row mt-5 pb-5">
    <div class="col text-center">
        <div class="block-27">
            <ul>
                <li><a href="#">&lt;</a></li>
                <li class="active"><span>1</span></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">&gt;</a></li>
            </ul>
        </div>
    </div>
</div> -->

<?php $pager->setSurroundCount(3) ?>

<div class="row mt-5 pb-5">
    <div class="col text-center">
        <div class="block-27">
            <nav aria-label="Page navigation">
                <ul class="pagination justify-content-center">
                    <?php if ($pager->hasPrevious()) : ?>
                        <li>
                            <a href="<?= $pager->getFirst() ?>" aria-label="<?= lang('Pager.first') ?>">
                                First
                            </a>
                        </li>
                        <!-- <li>
                            <a href="<?= $pager->getPrevious() ?>" aria-label="<?= lang('Pager.previous') ?>">
                                <span aria-hidden="true"><?= lang('Pager.previous') ?></span>
                            </a>
                        </li> -->
                    <?php endif ?>

                    <?php foreach ($pager->links() as $link) : ?>
                        <li <?= $link['active'] ? 'class="active"' : '' ?>>
                            <a href="<?= $link['uri'] ?>">
                                <?= $link['title'] ?>
                            </a>
                        </li>
                    <?php endforeach ?>

                    <?php if ($pager->hasNext()) : ?>
                        <!-- <li>
                            <a href="<?= $pager->getNext() ?>" aria-label="<?= lang('Pager.next') ?>">
                                <span aria-hidden="true"><?= lang('Pager.next') ?></span>
                            </a>
                        </li> -->
                        <li>
                            <a href="<?= $pager->getLast() ?>" aria-label="<?= lang('Pager.last') ?>">
                                Last
                            </a>
                        </li>
                    <?php endif ?>
                </ul>
            </nav>
        </div>
    </div>
</div>