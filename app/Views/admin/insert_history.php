<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title><?= $title; ?></title>
</head>

<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <form action="/admin_history/save" method="post">
                    <div class="form-row">
                        <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                        <input type="hidden" class="form-control <?= ($validation->hasError('merk')) ? 'is-invalid' : '' ?>" name="merk" id="merk" value="<?= old("merk"); ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("merk"); ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="nama_barang">Nama Barang</label>
                            <input type="text" class="form-control <?= ($validation->hasError('nama_barang')) ? 'is-invalid' : '' ?>" name="nama_barang" id="nama_barang" value="<?= old("nama_barang"); ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("nama_barang"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="merk">Merk</label>
                            <input type="text" class="form-control <?= ($validation->hasError('merk')) ? 'is-invalid' : '' ?>" name="merk" id="merk" value="<?= old("merk"); ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("merk"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="sumber">Source</label>
                            <input type="text" class="form-control <?= ($validation->hasError('sumber')) ? 'is-invalid' : '' ?>" name="sumber" id="sumber" value="<?= old("sumber"); ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("sumber"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="tujuan">Tujuan</label>
                            <input type="text" class="form-control <?= ($validation->hasError('tujuan')) ? 'is-invalid' : '' ?>" name="tujuan" id="tujuan" value="<?= old("tujuan"); ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("tujuan"); ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="harga_beli">Harga Beli</label>
                            <input type="text" class="form-control <?= ($validation->hasError('harga_beli')) ? 'is-invalid' : '' ?>" name="harga_beli" id="harga_beli" value="<?= old("harga_beli"); ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("harga_beli"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="harga_jual">Harga Jual</label>
                            <input type="text" class="form-control <?= ($validation->hasError('harga_jual')) ? 'is-invalid' : '' ?>" name="harga_jual" id="harga_jual" value="<?= old("harga_jual"); ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("harga_jual"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="total">Total</label>
                            <input type="text" class="form-control <?= ($validation->hasError('total')) ? 'is-invalid' : '' ?>" name="total" id="total" value="<?= old("total"); ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("total"); ?>
                            </div>
                        </div>
                    </div>

                    <button type="submit" name="tambah" class="btn btn-primary">Tambah Data</button>
                </form>

            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>

</html>