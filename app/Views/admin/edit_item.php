<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title><?= $title; ?></title>
</head>

<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <form action="/admin_item/update/<?= $item["id"]; ?>" method="post">
                    <div class="form-row">
                        <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                        <input type="hidden" name="nama_barang" class="form-control <?= ($validation->hasError("nama_barang")) ? 'is-invalid' : ''; ?>" id="nama_barang" autofocus value="<?= (old("nama_barang")) ? old("nama_barang") : $item["nama_barang"]; ?>">
                        <div class="form-group col-md-6">
                            <label for="nama_barang">Nama Barang</label>
                            <input type="text" name="nama_barang" class="form-control <?= ($validation->hasError("nama_barang")) ? 'is-invalid' : ''; ?>" id="nama_barang" autofocus value="<?= (old("nama_barang")) ? old("nama_barang") : $item["nama_barang"]; ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("nama_barang"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="merk">Merk</label>
                            <input type="text" class="form-control <?= ($validation->hasError('merk')) ? 'is-invalid' : '' ?>" name="merk" id="merk" value="<?= (old("merk")) ? old("merk") : $item["merk"]; ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("merk"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="kondisi">Kondisi</label>
                        <input type="text" class="form-control <?= ($validation->hasError('kondisi')) ? 'is-invalid' : '' ?>" name="kondisi" id="kondisi" placeholder="Input Gambar Name Here..." value="<?= (old("kondisi")) ? old("kondisi") : $item["kondisi"]; ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("kondisi"); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lokasi">Lokasi</label>
                        <input type="text" class="form-control <?= ($validation->hasError('lokasi')) ? 'is-invalid' : '' ?>" name="lokasi" id="lokasi" placeholder="Input Location Here..." value="<?= (old("lokasi")) ? old("lokasi") : $item["lokasi"]; ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("lokasi"); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="harga">Harga</label>
                            <input type="text" class="form-control <?= ($validation->hasError('harga')) ? 'is-invalid' : '' ?>" name="harga" id="harga" value="<?= (old("harga")) ? old("harga") : $item["harga"]; ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("harga"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="stok">Stok</label>
                            <input type="text" class="form-control <?= ($validation->hasError('stok')) ? 'is-invalid' : '' ?>" name="stok" id="stok" value="<?= (old("stok")) ? old("stok") : $item["stok"]; ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("stok"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="barang">Barang</label>
                            <input type="text" class="form-control <?= ($validation->hasError('barang')) ? 'is-invalid' : '' ?>" name="barang" id="barang" value="<?= (old("barang")) ? old("barang") : $item["barang"]; ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("barang"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="kontak">Kontak</label>
                            <input type="text" class="form-control <?= ($validation->hasError('kontak')) ? 'is-invalid' : '' ?>" name="kontak" id="kontak" value="<?= (old("kontak")) ? old("kontak") : $item["kontak"]; ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("kontak"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="gambar">Gambar</label>
                        <input type="text" class="form-control <?= ($validation->hasError('gambar')) ? 'is-invalid' : '' ?>" name="gambar" id="gambar" placeholder="Input Gambar Name Here..." value="<?= (old("gambar")) ? old("gambar") : $item["gambar"]; ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("gambar"); ?>
                        </div>
                    </div>
                    <button type="submit" name="tambah" class="btn btn-primary">Tambah Data</button>
                </form>

            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>

</html>