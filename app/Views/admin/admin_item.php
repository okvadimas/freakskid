<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/images/icon.jpg">
    <title>Freak Skid Admin Dashboard</title>
    <!-- This page CSS -->
    <!-- chartist CSS -->
    <link href="/assets/node_modules/morrisjs/morris.css" rel="stylesheet">
    <!--c3 plugins CSS -->
    <link href="/assets/node_modules/c3-master/c3.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/css/style_admin.css" rel="stylesheet">
    <!-- Dashboard 1 Page CSS -->
    <link href="/css/dashboard1.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="skin-default-dark fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elegant admin</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="admin_item">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="/images/logo-icon.png" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="/images/logo-light-icon.png" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                            <!-- dark Logo text -->
                            <img src="/images/logo-text.png" alt="homepage" class="dark-logo" />
                            <!-- Light Logo text -->
                            <img src="/images/logo-light-text.png" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto">
                        <!-- This is  -->
                        <li class="nav-item hidden-sm-up"> <a class="nav-link nav-toggler waves-effect waves-light" href="javascript:void(0)"><i class="ti-menu"></i></a></li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item search-box"> <a class="nav-link waves-effect waves-dark"><i class="fa fa-search"></i></a>
                            <form class="app-search" method="post">
                                <input type="text" name="keyword" class="form-control" placeholder="Search &amp; enter" value="<?= ($keyword) ? $keyword : "" ?>"><a class="srh-btn"><i class="fa fa-times"></i></a>
                            </form>
                        </li>
                    </ul>
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="/assets/images/users/1.jpg" alt="user" class="img-circle" width="30"></a>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <div class="d-flex no-block nav-text-box align-items-center">
                <span><img src="/images/logo-icon.png" alt="elegant admin template"></span>
                <a class="waves-effect waves-dark ml-auto hidden-sm-down" href="javascript:void(0)"><i class="ti-menu"></i></a>
                <a class="nav-toggler waves-effect waves-dark ml-auto hidden-sm-up" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
            </div>
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li> <a class="waves-effect waves-dark" href="/admin_history" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard History</span></a></li>
                        <li> <a class="waves-effect waves-dark" href="/admin_item" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard Item</span></a></li>
                        <li> <a class="waves-effect waves-dark" href="/admin_profile" aria-expanded="false"><i class="fa fa-user-circle-o"></i><span class="hide-menu">Profile</span></a></li>
                        <li class="disabled"> <a class="waves-effect waves-dark" href="table-basic.html" aria-expanded="false"><i class="fa fa-table"></i><span class="hide-menu"></span>Tables</a></li>
                        <li class="disabled"> <a class="waves-effect waves-dark" href="icon-fontawesome.html" aria-expanded="false"><i class="fa fa-smile-o"></i><span class="hide-menu"></span>Icon</a></li>
                        <li class="disabled"> <a class="waves-effect waves-dark" href="map-google.html" aria-expanded="false"><i class="fa fa-globe"></i><span class="hide-menu"></span>Map</a></li>
                        <li class="disabled"> <a class="waves-effect waves-dark" href="pages-blank.html" aria-expanded="false"><i class="fa fa-bookmark-o"></i><span class="hide-menu"></span>Blank</a></li>
                        <li class="disabled"> <a class="waves-effect waves-dark" href="pages-error-404.html" aria-expanded="false"><i class="fa fa-question-circle"></i><span class="hide-menu"></span>404</a></li>
                        <div class="text-center m-t-30">
                            <a href="/item" class="btn waves-effect waves-light btn-success hidden-md-down fa fa-sign-out"> Sign Out</a>
                        </div>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center d-flex">
                        <h4 class="text-themecolor align-self-center">Home</h4>
                        <!-- <a href=""><i class="fa fa-plus-square fa-2x breadcrumb-item active text-success" aria-hidden="true"></i></a> -->
                        <a class="btn btn-success d-none d-lg-block m-l-15 fa fa-plus-square" href="" data-toggle="modal" data-target="#ModalInput"> Input Data</a>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                            <a class="btn btn-success d-none d-lg-block m-l-15 fa fa-sign-out" href="/item"> Sign Out</a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <!-- column -->
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div>
                                        <h5 class="card-title">Monthly Report Overview</h5>
                                        <h6 class="card-subtitle">Check the monthly report </h6>
                                    </div>
                                    <div class="ml-auto">
                                        <select class="custom-select b-0">
                                            <option>January</option>
                                            <option value="1">February</option>
                                            <option value="2" selected="">March</option>
                                            <option value="3">April</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <?php if (session()->getFlashdata("pesan_gagal")) : ?>
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <?= session()->getFlashdata("pesan_gagal"); ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            <?php elseif (session()->getFlashdata("pesan")) : ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?= session()->getFlashdata("pesan"); ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            <?php endif; ?>

                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">No</th>
                                            <th>Nama Barang</th>
                                            <th>Merk</th>
                                            <th>Kondisi</th>
                                            <th>Lokasi</th>
                                            <th>Harga</th>
                                            <th>Stok</th>
                                            <th>Barang</th>
                                            <th>Kontak</th>
                                            <th>Gambar</th>
                                            <th class="text-center">ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1 + (12 * ($current_page - 1)); ?>
                                        <?php foreach ($items as $item) : ?>
                                            <tr>
                                                <td class="text-center txt-oflo"><?= $i++; ?></td>
                                                <td class="txt-oflo"><?= $item["nama_barang"]; ?></td>
                                                <td class="txt-oflo"><?= $item["merk"]; ?></td>
                                                <td class="txt-oflo"><?= $item["kondisi"]; ?></td>
                                                <td class="txt-oflo"><?= $item["lokasi"]; ?></td>
                                                <td class="txt-oflo"><?= $item["harga"]; ?></td>
                                                <td class="txt-oflo"><?= $item["stok"]; ?></td>
                                                <td class="txt-oflo"><span class="text-success"><?= $item["barang"]; ?></span></td>
                                                <td class="txt-oflo"><?= $item["kontak"]; ?></td>
                                                <td class="txt-oflo"><?= $item["gambar"]; ?></td>
                                                <td class="text-center">
                                                    <a href="/admin_item/edit/<?= $item["id"]; ?>" class="fa fa-pencil fa-2x pr-3"></a>
                                                    <a href="/admin_item/delete/<?= $item["id"]; ?>" class="fa fa-trash-o fa-2x" data-toggle="modal" data-target="#Modal"></a>
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="Modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel">Admin Penjualan Confirmation</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    Apakah anda yakin ingin menghapus data ini ?
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                                    <button type="submit" class="btn btn-primary" onclick="document.location.href = '/admin_item/delete/<?= $item['id']; ?>'">Confirm</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        <?php if (isset($null)) : ?>
                                            <tr>
                                                <td colspan="8">
                                                    <p align="center">Data is Not Found!</p>
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?= $pager->links("tabel_detail", "item_pagination_bootstrap"); ?>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


            <!-- ============================================================== -->
            <!-- Modal Data Center  -->
            <!-- ============================================================== -->

            <!-- Modals Insert -->
            <div class="modal fade" id="ModalInput" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Input New Data Penjualan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="/admin_item/save" method="post">
                            <div class="modal-body">
                                <div class="form-row">
                                    <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                                    <input type="hidden" class="form-control <?= ($validation->hasError('merk')) ? 'is-invalid' : '' ?>" name="merk" id="merk" value="<?= old("merk"); ?>">
                                    <div class="form-group col-md-6">
                                        <label for="nama_barang">Nama Barang</label>
                                        <input type="text" class="form-control <?= ($validation->hasError('nama_barang')) ? 'is-invalid' : '' ?>" name="nama_barang" id="nama_barang" value="<?= old("nama_barang"); ?>">
                                        <div class="invalid-feedback">
                                            <?= $validation->getError("nama_barang"); ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="merk">Merk</label>
                                        <input type="text" class="form-control <?= ($validation->hasError('merk')) ? 'is-invalid' : '' ?>" name="merk" id="merk" value="<?= old("merk"); ?>">
                                        <div class="invalid-feedback">
                                            <?= $validation->getError("merk"); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="kondisi">Kondisi</label>
                                    <input type="text" class="form-control <?= ($validation->hasError('kondisi')) ? 'is-invalid' : '' ?>" name="kondisi" id="kondisi" placeholder="Input Gambar Name Here..." value="<?= old("kondisi"); ?>">
                                    <div class="invalid-feedback">
                                        <?= $validation->getError("kondisi"); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="lokasi">Lokasi</label>
                                    <input type="text" class="form-control <?= ($validation->hasError('lokasi')) ? 'is-invalid' : '' ?>" name="lokasi" id="lokasi" placeholder="Input Location Here..." value="<?= old("lokasi"); ?>">
                                    <div class="invalid-feedback">
                                        <?= $validation->getError("lokasi"); ?>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="harga">Harga</label>
                                        <input type="text" class="form-control <?= ($validation->hasError('harga')) ? 'is-invalid' : '' ?>" name="harga" id="harga" value="<?= old("harga"); ?>">
                                        <div class="invalid-feedback">
                                            <?= $validation->getError("harga"); ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="stok">Stok</label>
                                        <input type="text" class="form-control <?= ($validation->hasError('stok')) ? 'is-invalid' : '' ?>" name="stok" id="stok" value="<?= old("stok"); ?>">
                                        <div class="invalid-feedback">
                                            <?= $validation->getError("stok"); ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="barang">Barang</label>
                                        <input type="text" class="form-control <?= ($validation->hasError('barang')) ? 'is-invalid' : '' ?>" name="barang" id="barang" value="<?= old("barang"); ?>">
                                        <div class="invalid-feedback">
                                            <?= $validation->getError("barang"); ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="kontak">Kontak</label>
                                        <input type="text" class="form-control <?= ($validation->hasError('kontak')) ? 'is-invalid' : '' ?>" name="kontak" id="kontak" value="<?= old("kontak"); ?>">
                                        <div class="invalid-feedback">
                                            <?= $validation->getError("kontak"); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="gambar">Gambar</label>
                                    <input type="text" class="form-control <?= ($validation->hasError('gambar')) ? 'is-invalid' : '' ?>" name="gambar" id="gambar" placeholder="Input Gambar Name Here..." value="<?= old("gambar"); ?>">
                                    <div class="invalid-feedback">
                                        <?= $validation->getError("gambar"); ?>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button type="submit" name="tambah" class="btn btn-primary">Tambah Data</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2018 Elegent Admin by wrappixel.com
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap popper Core JavaScript -->
    <script src="/assets/node_modules/popper/popper.min.js"></script>
    <script src="../assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!--morris JavaScript -->
    <script src="/assets/node_modules/raphael/raphael-min.js"></script>
    <script src="/assets/node_modules/morrisjs/morris.min.js"></script>
    <script src="/assets/node_modules/jquery-sparkline/jquery.sparkline.min.js"></script>
    <!--c3 JavaScript -->
    <script src="/assets/node_modules/d3/d3.min.js"></script>
    <script src="/assets/node_modules/c3-master/c3.min.js"></script>
    <!-- Chart JS -->
    <script src="/js/dashboard1.js"></script>
</body>

</html>