<?php

namespace App\Models;

use CodeIgniter\Model;

class Models extends Model
{
    protected $table      = 'tabel_detail';
    protected $primaryKey = 'id';
    protected $allowedFields = ['nama_barang', 'merk', 'kondisi', 'lokasi', 'harga', 'stok', 'barang', 'kontak', 'gambar'];
    protected $useTimestamps = true;

    public function search($keyword)
    {
        // ini ada di ci4 secara default namanya query builder looking dor similar data
        // cara default
        // $builder = $this->table("tabel_detail");
        // $builder->like("merk", $keyword);

        // cara cepat
        return $this->table("tabel_detail")->like("merk", "$keyword")->orLike("barang", $keyword)->orLike("lokasi", $keyword)->orLike("nama_barang", $keyword);
    }
}
