<?php

namespace App\Models;

use CodeIgniter\Model;

class Models_penjualan extends Model
{
    protected $table      = 'tabel_penjualan';
    protected $primaryKey = 'id';
    protected $allowedFields = ['nama_barang', 'merk', 'total', 'sumber', 'tujuan', 'harga_beli', 'harga_jual', 'waktu'];

    public function search($keyword)
    {
        // ini ada di ci4 secara default namanya query builder looking dor similar data
        // cara default
        // $builder = $this->table("tabel_detail");
        // $builder->like("merk", $keyword);

        // cara cepat
        return $this->table("tabel_penjualan")->like("merk", "$keyword")->orLike("nama_barang", $keyword)->orLike("tujuan", $keyword)->orLike("sumber", $keyword);
    }
}
