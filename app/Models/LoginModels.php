<?php

namespace App\Models;

use CodeIgniter\Model;

class LoginModels extends Model
{
    protected $table      = 'orang';
    protected $primaryKey = 'id';
    protected $allowedFields = ['username', 'email', 'password', 'level'];
}
