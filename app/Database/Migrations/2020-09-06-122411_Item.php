<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Item extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'          => [
				'type'           => 'INT',
				'constraint'     => 11,
				'auto_increment' => true,
			],
			'nama_barang'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '200',
			],
			'merk' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'kondisi' => [
				'type'           => 'VARCHAR',
				'constraint'     => '255',
			],
			'lokasi' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'harga' => [
				'type'           => 'VARCHAR',
				'constraint'     => '50',
			],
			'stok' => [
				'type'           => 'VARCHAR',
				'constraint'     => '50',
			],
			'barang' => [
				'type'           => 'VARCHAR',
				'constraint'     => '50',
			],
			'kontak' => [
				'type'           => 'VARCHAR',
				'constraint'     => '255',
			],
			'created_at' => [
				'type'           => 'DATETIME',
				'null'     		 => true,
			],
			'updated_at' => [
				'type'           => 'DATETIME',
				'null'    		 => true,
			],
			'gambar' => [
				'type'           => 'VARCHAR',
				'constraint'     => '255',
			],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('tabel_detail');
	}

	public function down()
	{
		$this->forge->dropTable('tabel_detail');
	}
}
