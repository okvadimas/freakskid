<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class History extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'          => [
				'type'           => 'INT',
				'constraint'     => 11,
				'auto_increment' => true,
			],
			'nama_barang'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '200',
			],
			'merk' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'total' => [
				'type'           => 'VARCHAR',
				'constraint'     => '255',
			],
			'sumber' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'tujuan' => [
				'type'           => 'VARCHAR',
				'constraint'     => '50',
			],
			'harga_beli' => [
				'type'           => 'VARCHAR',
				'constraint'     => '50',
			],
			'harga_jual' => [
				'type'           => 'VARCHAR',
				'constraint'     => '50',
			],
			'waktu' => [
				'type'           => 'VARCHAR',
				'constraint'     => '255',
			],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('tabel_penjualan');
	}

	public function down()
	{
		$this->forge->dropTable('tabel_penjualan');
	}
}
