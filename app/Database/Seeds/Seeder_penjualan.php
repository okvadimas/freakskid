<?php

namespace App\Database\Seeds;

use CodeIgniter\I18n\Time;

class Seeder_penjualan extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        $faker = \Faker\Factory::create();

        // $data = [
        //     'nama_barang' => 'Nama Barang',
        //     'merk'    => 'Merek',
        //     'kondisi'    => $faker->text,
        //     'lokasi'    => $faker->address,
        //     'harga'    => '100k',
        //     'stok'    => '2',
        //     'barang'    => 'ready',
        //     'kontak'    => '088211234125',
        //     'created_at'    => Time::createFromTimestamp($faker->unixTime()),
        //     'updated_at'    => Time::createFromTimestamp($faker->unixTime()),
        //     'gambar'    => 'image_1.jpg',
        // ];

        for ($i = 1; $i <= 50; $i++) {
            $data = [
                'nama_barang' => 'Nama Barang',
                'merk'    => 'Merk',
                'total'    => 'Total',
                'sumber'    => 'Sumber',
                'tujuan'    => 'Tujuan',
                'harga_beli'    => 'Harga Beli',
                'harga_jual'    => 'Harga Jual',
                'waktu'    => Time::now(),
            ];
            $this->db->table('tabel_penjualan')->insert($data);
        }


        // Simple Queries
        // $this->db->query(
        //     "INSERT INTO users (username, email) VALUES(:username:, :email:)",
        //     $data
        // );

        // Using Query Builder
        // $this->db->table('tabel_detail')->insert($data);
    }
}
