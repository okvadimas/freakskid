<?php

namespace App\Database\Seeds;

use CodeIgniter\I18n\Time;

class Seeder_orang extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        $faker = \Faker\Factory::create();

        $data = [
            [
                'username' => 'admin',
                'email'    => 'admin@gmail.com',
                'password'    => 'admin',
                'level'    => '1',
            ],
            [
                'username' => 'user',
                'email'    => 'user@gmail.com',
                'password'    => 'user',
                'level'    => '2',
            ],
        ];

        // Simple Queries
        // $this->db->query(
        //     "INSERT INTO users (username, email) VALUES(:username:, :email:)",
        //     $data
        // );

        // Using Query Builder
        $this->db->table('orang')->insertBatch($data);
    }
}
