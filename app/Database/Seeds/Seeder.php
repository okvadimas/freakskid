<?php

namespace App\Database\Seeds;

use CodeIgniter\I18n\Time;

class Seeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        $faker = \Faker\Factory::create();

        // $data = [
        //     'nama_barang' => 'Nama Barang',
        //     'merk'    => 'Merek',
        //     'kondisi'    => $faker->text,
        //     'lokasi'    => $faker->address,
        //     'harga'    => '100k',
        //     'stok'    => '2',
        //     'barang'    => 'ready',
        //     'kontak'    => '088211234125',
        //     'created_at'    => Time::createFromTimestamp($faker->unixTime()),
        //     'updated_at'    => Time::createFromTimestamp($faker->unixTime()),
        //     'gambar'    => 'image_1.jpg',
        // ];

        for ($i = 1; $i <= 50; $i++) {
            $data = [
                'nama_barang' => 'Nama Barang',
                'merk'    => 'Merk',
                'kondisi'    => 'Kondisi',
                'lokasi'    => 'Lokasi',
                'harga'    => 'Harga',
                'stok'    => 'Stok',
                'barang'    => 'Ready or Sold',
                'kontak'    => 'Nomer Wa or Sosmed',
                'created_at'    => Time::now(),
                'updated_at'    => Time::now(),
                'gambar'    => 'image_1.jpg',
            ];
            $this->db->table('tabel_detail')->insert($data);
        }


        // Simple Queries
        // $this->db->query(
        //     "INSERT INTO users (username, email) VALUES(:username:, :email:)",
        //     $data
        // );

        // Using Query Builder
        // $this->db->table('tabel_detail')->insert($data);
    }
}
